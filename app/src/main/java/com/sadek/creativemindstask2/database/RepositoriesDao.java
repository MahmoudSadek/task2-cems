package com.sadek.creativemindstask2.database;


import com.sadek.creativemindstask2.model.Repository;

import io.realm.Realm;

public class RepositoriesDao extends GenericDao<Repository> {

    private static RepositoriesDao repositoriesDao;

    private RepositoriesDao() {
        super();
    }

    public static RepositoriesDao getInstance() {

        if (repositoriesDao == null)
            repositoriesDao = new RepositoriesDao();
        return repositoriesDao;
    }


    @Override
    void extractInverselyLinkedData(Realm realm, Repository item, Repository result) {

    }
}
