package com.sadek.creativemindstask2.presenter;

import android.content.Context;

import com.sadek.creativemindstask2.interfaces.ReposInterface;
import com.sadek.creativemindstask2.model.Repository;
import com.sadek.creativemindstask2.service.ServiceBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepostoriesPresenter {

    public static void getReposResponse(final Context context,
                                        int page,
                                        final ReposInterface myInterface) {
        myInterface.showProgress(true);

        Call<List<Repository>> myCall = ServiceBuilder.getRouter(context).listRepositories("square"
                , page + "", "10");
        myCall.enqueue(new Callback<List<Repository>>() {

            @Override
            public void onResponse(Call<List<Repository>> call, Response<List<Repository>> response) {
                myInterface.showProgress(false);
                if (response.isSuccessful()) {
                    myInterface.onSuccess(response.body());
                } else {
                    myInterface.onErorr(response.errorBody().toString());

                }

            }

            @Override
            public void onFailure(Call<List<Repository>> call, Throwable t) {
                myInterface.showProgress(false);
                myInterface.onErorr(t.getMessage());
            }
        });

    }

}
