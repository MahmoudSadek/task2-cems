package com.sadek.creativemindstask2.interfaces;


import com.sadek.creativemindstask2.model.Repository;

import java.util.List;

public interface ReposInterface {

    void showProgress(boolean show);

    void onErorr(String response);

    void onSuccess(List<Repository> response);

}
