package com.sadek.creativemindstask2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class OwnerEntity extends RealmObject {
    @Expose
    @SerializedName("login")
    private String login;
    @Expose
    @SerializedName("html_url")
    private String html_url;

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
