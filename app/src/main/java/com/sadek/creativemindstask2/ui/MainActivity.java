package com.sadek.creativemindstask2.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sadek.creativemindstask2.R;
import com.sadek.creativemindstask2.database.RepositoriesDao;
import com.sadek.creativemindstask2.interfaces.ReposInterface;
import com.sadek.creativemindstask2.model.Repository;
import com.sadek.creativemindstask2.presenter.RepostoriesPresenter;
import com.sadek.creativemindstask2.service.ApplicationController;
import com.sadek.creativemindstask2.ui.adapter.RepostoryAdapter;
import com.sadek.creativemindstask2.ui.dialog.PressDialog;
import com.sadek.creativemindstask2.utils.AlarmReceiver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements ReposInterface, RepostoryAdapter.OnClickItem,
        RepostoryAdapter.Callbacks {

    @BindView(R.id.repos_list_view)
    RecyclerView repos_list_view;
    @BindView(R.id.loading_progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.search_txt)
    EditText search_txt;
    RepostoryAdapter adapter;

    private List<Repository> repos;
    private PendingIntent pendingIntent;
    private AlarmManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        repos=new ArrayList<>();
        setUIValues();
        initialize();


        startAlarm();
    }
    public void startAlarm() {

        // Retrieve a PendingIntent that will perform a broadcast
        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        int interval = 400;

        manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), AlarmManager.INTERVAL_HOUR, pendingIntent);
//        Toast.makeText(this, "Alarm Set", Toast.LENGTH_SHORT).show();
        Log.d("sadek", "startAlarm: Alarm Set");

    }

    private void initialize() {
        // Fetch repos

        if (!ApplicationController.isNetworkAvailable()) {
            // bring it from realm
            RepositoriesDao repositoriesDao = RepositoriesDao.getInstance();
            repos = repositoriesDao.findAll();
            setUIValues();
        } else {
            page=1;
            progressBar.setVisibility(View.VISIBLE);
            RepostoriesPresenter.getReposResponse(this, page,this);
        }
    }

    private void setUIValues() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        repos_list_view.setLayoutManager(linearLayoutManager);


        adapter = new RepostoryAdapter(this, repos, this);
        repos_list_view.setAdapter(adapter);

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_bright);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initialize();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        swipeRefreshLayout.setRefreshing(false);

        search_txt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });
        search_txt.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() == 0)
                    initialize();
                else performSearch();

            }
        });
    }

    private void performSearch() {
        if (!search_txt.getText().toString().isEmpty()) {

            List<Repository> reposFilter = new ArrayList<>();
            for (int i = 0; i < repos.size(); i++) {
                String Searched = repos.get(i).getName()+" "+repos.get(i).getOwner().getLogin();
                if(Searched.toLowerCase().contains(search_txt.getText().toString().toLowerCase()))
                    reposFilter.add(repos.get(i));
            }
            adapter = new RepostoryAdapter(this, reposFilter, this);
            repos_list_view.setAdapter(adapter);
            page=1;
        }
    }


    @Override
    public void showProgress(boolean show) {
        if (show)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onErorr(String response) {

    }

    @Override
    public void onSuccess(List<Repository> response) {
        try {

            if (page == 1) {
                repos.clear();
                adapter = new RepostoryAdapter(this, repos, this);
                repos_list_view.setAdapter(adapter);
            }
            repos.addAll(response);

            adapter.setCallback(this);
            if (response.size() == num)
                adapter.setWithFooter(true); //enabling footer to show
            adapter.notifyDataSetChanged();

            // save data into realm
            RepositoriesDao repositoriesDao = RepositoriesDao.getInstance();
            repositoriesDao.deleteAll(null, null);
            repositoriesDao.insertOrUpdate(repos);

        } catch (Exception e) {
        }
    }


    PressDialog pressDialog;
    @Override
    public void onItemClicked(View view, Repository model) {

        pressDialog = new PressDialog(this, new PressDialog.CheckAction() {
            @Override
            public void onGetAction(int code) {
                pressDialog.dismiss();
                Intent browserIntent;
                if (code==1) {
                    browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(model.getHtml_url()));
                } else {
                    browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(model.getOwner().getHtml_url()));
                }
                startActivity(browserIntent);
            }
        });
        pressDialog.show();
    }
    int page = 1, num = 10;
    @Override
    public void onClickLoadMore() {
        adapter.setWithFooter(false); // hide footer
        page++;
        RepostoriesPresenter.getReposResponse(this, page,this);

        adapter.notifyDataSetChanged(); // more elements will be added
    }
}