package com.sadek.creativemindstask2.service;

import com.sadek.creativemindstask2.model.Repository;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Router {

    @GET("users/{user}/repos")
    Call<List<Repository>> listRepositories(@Path("user") String user, @Query("page") String page, @Query("per_page") String per_page );


}
