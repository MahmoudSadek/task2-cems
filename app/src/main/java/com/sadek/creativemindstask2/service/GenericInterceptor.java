package com.sadek.creativemindstask2.service;

import android.content.Context;


import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


public class GenericInterceptor implements Interceptor {
    private Context context;

    public GenericInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        // Delay Requests with development
//        SystemClock.sleep(BuildConfig.DELAY);

        return addRequestHeaders(chain, context);
    }

    private static Response addRequestHeaders(Chain chain, Context context) throws IOException {
        Request request;

        request = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json")
                .build();
        return chain.proceed(request);
    }
}
//0c5545c2fd5ab4bed296e786b1d805d8
