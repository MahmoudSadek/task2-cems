package com.sadek.creativemindstask2.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.sadek.creativemindstask2.database.RepositoriesDao;
import com.sadek.creativemindstask2.interfaces.ReposInterface;
import com.sadek.creativemindstask2.model.Repository;
import com.sadek.creativemindstask2.presenter.RepostoriesPresenter;
import com.sadek.creativemindstask2.ui.adapter.RepostoryAdapter;

import java.util.List;

public class AlarmReceiver extends BroadcastReceiver implements ReposInterface {

    @Override
    public void onReceive(Context context, Intent arg1) {
        // For our recurring task, we'll just display a message
//        Toast.makeText(context, "I'm running"+System.currentTimeMillis(), Toast.LENGTH_SHORT).show();
        Log.d("sadek", "startAlarm: I'm running"+System.currentTimeMillis());

        RepostoriesPresenter.getReposResponse(context, 1,this);
    }

    @Override
    public void showProgress(boolean show) {

    }

    @Override
    public void onErorr(String response) {

    }

    @Override
    public void onSuccess(List<Repository> response) {

        // save data into realm
        RepositoriesDao repositoriesDao = RepositoriesDao.getInstance();
        repositoriesDao.deleteAll(null, null);
        repositoriesDao.insertOrUpdate(response);

    }
}