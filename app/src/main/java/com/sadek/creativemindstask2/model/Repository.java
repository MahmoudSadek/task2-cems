package com.sadek.creativemindstask2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Repository extends RealmObject {

    @Expose
    @SerializedName("description")
    private String description;
    @Expose
    @SerializedName("html_url")
    private String html_url;
    @Expose
    @SerializedName("fork")
    private boolean fork;
    @Expose
    @SerializedName("owner")
    private OwnerEntity owner;
    @Expose
    @SerializedName("full_name")
    private String fullName;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("node_id")
    private String nodeId;
    @Expose
    @SerializedName("id")
    private int id;

    public boolean isFork() {
        return fork;
    }

    public void setFork(boolean fork) {
        this.fork = fork;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OwnerEntity getOwner() {
        return owner;
    }

    public void setOwner(OwnerEntity owner) {
        this.owner = owner;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
