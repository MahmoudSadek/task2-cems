package com.sadek.creativemindstask2.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sadek.creativemindstask2.R;
import com.sadek.creativemindstask2.model.Repository;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepostoryAdapter extends RecyclerView.Adapter {

    private List<Repository> contents;
    private Context mContext;
    OnClickItem mOnClickItem;

    public interface Callbacks {
        public void onClickLoadMore();
    }

    private Callbacks mCallbacks;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 3;

    private boolean mWithHeader = false;
    private boolean mWithFooter = false;

    public RepostoryAdapter(Context mContext, List<Repository> contents, OnClickItem mOnClickItem) {
        this.contents = contents;
        this.mContext = mContext;
        this.mOnClickItem = mOnClickItem;
    }


    @Override
    public int getItemCount() {
        int itemCount = contents.size();
        if (mWithHeader)
            itemCount++;
        if (mWithFooter)
            itemCount++;
        return itemCount;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        try {
        if (viewType == TYPE_FOOTER) {

            view = View.inflate(parent.getContext(), R.layout.row_loadmore, null);
            return new LoadMoreViewHolder(view);

        } else

                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item_repository, parent, false);
                return new ViewHolderProduct(view,mOnClickItem);



        } catch (Exception e) {

            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_repository, parent, false);

            return new ViewHolder(view,mOnClickItem);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder mholder, final int position) {
        try {

            if (mholder instanceof LoadMoreViewHolder) {

                LoadMoreViewHolder loadMoreViewHolder = (LoadMoreViewHolder) mholder;

                loadMoreViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mCallbacks != null)
                            mCallbacks.onClickLoadMore();
                    }
                });

            } else {
                    try {
                        ViewHolderProduct holder = (ViewHolderProduct) mholder;
//                        Picasso.with(mContext).load(contents.get(position).getImages().get(0)).into(holder.home_product_image);
                        holder.repo_name.setText(contents.get(position).getName());
                        holder.repo_description.setText(contents.get(position).getDescription());
                        holder.user_name.setText(contents.get(position).getOwner().getLogin());
                        if(contents.get(position).isFork())
                            holder.card_view.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                        else
                            holder.card_view.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_green_light));
                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
//                                Bundle bundle = new Bundle();
//                                bundle.putString(Common.PRODUCT, contents.get(position).getId()+"");
//                                ((MainActivity) mContext).switchToPage(3, bundle, R.string.app_name);
                            }
                        });

                    } catch (Exception e) {

                    }


            }
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mWithHeader && isPositionHeader(position))
            return TYPE_HEADER;
        if (mWithFooter && isPositionFooter(position))
            return TYPE_FOOTER;

        return TYPE_ITEM;
    }

    public boolean isPositionHeader(int position) {
        return position == 0 && mWithHeader;
    }

    public boolean isPositionFooter(int position) {
        return position == getItemCount() - 1 && mWithFooter;
    }

    public void setWithHeader(boolean value) {
        mWithHeader = value;
    }

    public void setWithFooter(boolean value) {
        mWithFooter = value;
    }

    public void setCallback(Callbacks callbacks) {
        mCallbacks = callbacks;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView, OnClickItem mOnClickItem) {
            super(itemView);


        }


    }
    class ViewHolderProduct extends ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        @BindView(R.id.repo_name)
        TextView repo_name;
        @BindView(R.id.repo_description)
        TextView repo_description;
        @BindView(R.id.user_name)
        TextView user_name;
        @BindView(R.id.card_view)
        View card_view;


        private OnClickItem mOnClickItem;

        ViewHolderProduct(View view, OnClickItem mOnClickItem) {
            super(view, mOnClickItem);
            int position = getAdapterPosition();
            ButterKnife.bind(this, view);
            this.mOnClickItem = mOnClickItem;
            itemView.setOnLongClickListener(this);
            try {
                repo_name.setText(contents.get(position).getName());
                repo_description.setText(contents.get(position).getDescription());
                user_name.setText(contents.get(position).getOwner().getLogin());
                if(contents.get(position).isFork())
                card_view.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                else
                card_view.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_green_light));
            } catch (Exception e) {

            }
        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public boolean onLongClick(View v) {

            int position = getAdapterPosition();

            mOnClickItem.onItemClicked(itemView, contents.get(position));
            return true;
        }
    }
    public interface OnClickItem {
        void onItemClicked(View view, Repository model);
    }


    public class LoadMoreViewHolder extends RecyclerView.ViewHolder {

        public LoadMoreViewHolder(View itemView) {
            super(itemView);
        }
    }

}