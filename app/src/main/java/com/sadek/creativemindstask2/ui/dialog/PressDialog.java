package com.sadek.creativemindstask2.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.sadek.creativemindstask2.R;


public class PressDialog extends Dialog {
    CheckAction checkAction;
    View repository, owner,back;
    public PressDialog(@NonNull final Context context, final CheckAction checkAction) {
        super(context);
        setContentView(R.layout.dialog_press);
        this.checkAction=checkAction;
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        repository=findViewById(R.id.repository);
        owner=findViewById(R.id.owner);
        back=findViewById(R.id.back_btn);
        owner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkAction.onGetAction(2);

            }
        }); repository.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkAction.onGetAction(1);

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public interface CheckAction{
        void onGetAction(int code);
    }
}
