package com.sadek.creativemindstask2.database;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RealmController {

    private static final int SCHEMA_VERSION = 0;
    private static final String DATABASE_NAME = "repo.realm";

    public static void init(Context context) {
        Realm.init(context);
        Realm.setDefaultConfiguration(defaultConfiguration());
    }

    private static RealmConfiguration defaultConfiguration() {
        return new RealmConfiguration.Builder()
                .name(DATABASE_NAME)
                .schemaVersion(SCHEMA_VERSION)
                .deleteRealmIfMigrationNeeded()             // TODO : support migrations before release
                .build();
    }
}
